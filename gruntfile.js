module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-execute');

    grunt.initConfig({
        sass: {
            options: {
                sourceMap: false
            },
            dist: {
                files: [{
                    expand: true,
                    src: ['packages/skyboard/widgets/**/*.scss'],
                    ext: '.css'
                }]
            }
        },

        watch: {
            source: {
                files: ['packages/skyboard/widgets/**/*.scss'],
                tasks: ['sass']
            }
        },

        execute: {
            target: {
                src: ['start.js']
            }
        },

        concurrent: {
            target: {
                tasks: [['sass', 'watch'], 'execute'],
                options: {
                    logConcurrentOutput: true
                }
            }
        }
    });

    grunt.registerTask('start', ['concurrent:target']);
};