This is the project for the Shopping team Dashboard package.

It uses Atlasboard.

Read the wiki for a complete guide on how it works:

https://bitbucket.org/atlassian/atlasboard/wiki/Home

In order to use this package you first need to create an empty Atlasboard dashboard, then you can install this package to this new dashboard

# To create a new Dashboard:
	atlasboard new dashboard
	cd dashboard
	rm -rf packages/demo
	rm -rf packages/default

# To Install this package

You can install packages easily as git submodules.

To install the atlasboard-hcom-package, just type this (in your dashboard's folder):

    git init
    git submodule add http://stash.hcom/scm/shp/teamdashboard.git packages/hcom
	
# Deployment

The Dashboard is deployed on Raspberry pi which can be logged into remotely:

	ssh pi@172.21.144.171
	raspberry
	
## To stop the dashboard if it's running:
	ps -ef | grep atlas
	kill -9 [pid]

## To start the dashboard, run these commands after login:
	cd dashboard
	nohup atlasboard start > /dev/null 2>&1 &
	
### You can then view the Dashboard by going to the following Url:
http://172.21.144.171:3000
	
	