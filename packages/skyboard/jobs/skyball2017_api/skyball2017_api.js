/**
 * Job: skyball2017_api
 *
 * Expected configuration:
 *
 * {
 *    "interval": 60000,
 *    "widgetTitle": "Csapatok",
 *    "url": "teams"
 * }
 */

var http = require('http');

module.exports = function (config, dependencies, job_callback) {
    var options = {url: 'http://skyathlon.tk/skyball-2017/api/' + config.url};
    var logger = dependencies.logger;

    dependencies.request(options, function(err, response, respData) {
        if (err || !response || response.statusCode !== 200) {
            var error_msg = (err || (response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + options.url;
            logger.error(error_msg);
            job_callback(error_msg);
        }
        else {
            var data = JSON.parse(respData);

            job_callback(null, {data: data, conf: config});
        }
    });
};
