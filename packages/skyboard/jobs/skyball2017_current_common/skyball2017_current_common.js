/**
 * Job: skyball2017_current_common
 *
 * Expected configuration:
 *
 * {
 *     "interval": 1000,
 *     "dateFormat": "YYYY-MM-DD"
 * }
 */

module.exports = {
    /**
     * Executed every interval
     * @param config
     * @param dependencies
     * @param jobCallback
     */
    onRun: function (config, dependencies, jobCallback) {
        var dateFormat = config.dateFormat ? config.dateFormat : "YYYY-MM-DD",
            currentDate = new Date();

        jobCallback(null, {
            date: currentDate,
            dateFormat: dateFormat
        });
    }
};
