/**
 * Job: skyball2017_news
 *
 * Expected configuration:
 *
 "skyball2017_news": {
    "interval": 10000,
    "widgetTitle": "Water SkyBall 2017 - hírek"
 }
 */

var redis = require("redis"),
    client = redis.createClient();

module.exports = function (config, dependencies, job_callback) {
    client.get('skyball2017_news', function(err, data) {
        job_callback(null, {data: data});
    });
};
