/**
 * Job: skyball2017_live_stats
 *
 * Expected configuration:
 *
 * {
        "interval": 10000,
        "widgetTitle": "XY szakasz valamije",
        "type": "XY.Z"
 * }
 */

var redis = require("redis"),
    client = redis.createClient();

module.exports = function (config, dependencies, job_callback) {
    client.get('livestats.' + config.type, function(err, data) {
        try {
            data = JSON.parse(data);
        }
        catch (e) {
            return job_callback(e);
        }
        
        client.get('settings', function(err, settings) {
            try {
                var dataToSend,
                    truetype;

                settings = JSON.parse(settings);
                truetype = config.type.substring(0, config.type.indexOf('.'));
                settings.currentType = truetype;

                client.get('livestats.' + truetype + '.ended', function(err, ended) {
                    settings.ended = JSON.parse(ended);

                    function orderObject(obj) {
                        var keys   = Object.keys(obj),
                            newobj = {},
                            k,
                            i, len = keys.length;

                        keys.sort();

                        for (i = 0; i < len; i++) {
                            k = keys[i];
                            newobj[k] = obj[k];
                        }

                        return newobj;
                    }

                    if (!config.dontOrder && data) {
                        data = orderObject(data);
                    }

                    dataToSend = {data: data, title: config.widgetTitle, type: config.type, settings: settings};
                    if (config.notit) {
                        dataToSend.notit = true;
                    }
                    if (config.line) {
                        dataToSend.line = config.line;
                    }
                    if (config.setTitle) {
                        dataToSend.setTitle = true;
                    }

                    job_callback(null, dataToSend);
                });
            }
            catch (e) {
                return job_callback(e);
            }
        });
    });
};
