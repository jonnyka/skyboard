/**
 * Job: skyball2017_time
 *
 * Expected configuration:
 *
 * {
 *    "interval": 60000,
 * }
 */

var http = require('http');

module.exports = function (config, dependencies, job_callback) {
    var options = {url: 'http://skyathlon.tk/api/time'};
    var logger = dependencies.logger;

    dependencies.request(options, function(err, response, respData) {
        if (err || !response || response.statusCode !== 200) {
            var error_msg = (err || (response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + options.url;
            logger.error(error_msg);
            job_callback(error_msg);
        }
        else {
            var data;

            try {
                data = JSON.parse(respData);
            }
            catch (e) {
                return job_callback(err);
            }
            job_callback(null, {data: data});
        }
    });
};
