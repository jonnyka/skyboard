/**
 * Job: skyball2016_current_teams
 *
 * Expected configuration:
 *
 * {
 *    "interval": 60000,
 *    "widgetTitle": "Csapatok"
 * }
 */

var http = require('http');

module.exports = function (config, dependencies, job_callback) {
    var options = {url: 'http://skyathlon.tk/skyball-2016/api/teams'};
    var logger = dependencies.logger;

    dependencies.request(options, function(err, response, respData) {
        if (err || !response || response.statusCode != 200) {
            var error_msg = (err || (response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + options.url;
            logger.error(error_msg);
            job_callback(error_msg);
        }
        else {
            var data;

            try {
                data = JSON.parse(respData);
            }
            catch (e){
                return job_callback(err);
            }
            job_callback(null, {data: data});
        }
    });
};
