/**
 * Job: skyball2016_current_common
 *
 * Expected configuration:
 *
 * {
 *     "interval": 1000,
 *     "dateFormat": "YYYY-MM-DD",
 *     "timeFormat": "HH:mm:ss"
 * }
 */

module.exports = {
    /**
     * Executed every interval
     * @param config
     * @param dependencies
     * @param jobCallback
     */
    onRun: function (config, dependencies, jobCallback) {
        var dateFormat = config.dateFormat ? config.dateFormat : "YYYY-MM-DD",
            timeFormat = config.timeFormat ? config.timeFormat : "HH:mm:ss",
            currentDate = new Date();

        jobCallback(null, {
            date: currentDate,
            dateFormat: dateFormat,
            timeFormat: timeFormat
        });
    }
};
