widget = {
    onData: function(el, d) {
        var content = '<div id="games"><div class="pull-left">',
            title = d.title,
            games = d.data,
            i,
            j = 1,
            type = d.type,
            iscurrent = (type.indexOf('.games') > -1),
            game,
            pointA,
            pointB,
            classA,
            classB,
            teamA,
            teamB,
            teamAname,
            teamBname,
            winnerA,
            winnerB,
            teamAcompName,
            teamBcompName,
            ref,
            sref,
            jref,
            currentClass,
            nopoint = '-',
            vs = '-',
            t,
            mainTitle = $('.title'),
            settings = d.settings,
            currentGameId = settings.currentGameId,
            currentRound = settings.currentRoundId,
            nextRound = 0,
            currentType  = settings.currentType,
            line = d.line,
            ended = settings.ended,
            titleStatus = ended ? ' (befejezett)' : ' (folyamatban)',
            gamesNum = parseInt(Object.keys(games).length),
            logosize = '50',
            height = ' style="height: calc(100% / ' + gamesNum + ')"';

        if (gamesNum < 6) {
            logosize = '80';
        }
        else if (gamesNum >= 12) {
            logosize = '32';
        }

        if (type === 'S.Nm' || type === 'RAJ.games') {
            logosize = '26';
        }

        if (type === 'RAJ.nRAJm') {
            logosize = '32';
        }

        if (type === 'S.Nm' || type === 'N.KDm' || type === 'KD.NDm' || type === 'ND.Dm') {
            titleStatus = ended ? ' (végleges)' : ' (nem végleges)'
        }

        if (d.notit) {
            titleStatus = '';
        }
        
        if (d.setTitle) {
            t = 'Water SkyBall 2017 - ' + currentRound + '. forduló';

            switch (type) {
                case 'RAJ.games':
                    if (currentRound == 17) {
                        t = 'Water SkyBall 2017 - Nagy Döntő';
                    }
                    else {
                        t = 'Water SkyBall 2017 - Rájátszás - ' + (+currentRound - +12) + '. kör';
                    }
                    break;
                case 'NUL.games':
                    t = 'Water SkyBall 2017 - 0. forduló';
                    break;
                case 'ES.games':
                    nextRound = currentRound + +1;
                    t = 'Water SkyBall 2017 - ' + currentRound + '. forduló előselejtező (' + nextRound + '. fordulóra)';
                    break;
                case 'S.games':
                    t += ' selejtező';
                    break;
                case 'N.games':
                    t += ' normál szakasz';
                    break;
                case 'KD.games':
                    t += ' középdöntő';
                    break;
                case 'ND.games':
                    t += ' negyeddöntő';
                    break;
                case 'D.games':
                    t += ' döntő';
                    break;
            }

            if (mainTitle.find('.time').length) {
               mainTitle.html(t + mainTitle.find('.time')[0].outerHTML);
            }
            else {
                mainTitle.text(t);
            }
        }

        if (type == 'RAJ.games') {
            switch (currentRound) {
                case 13:
                    line = 5;
                    break;
                case 14:
                    line = 7;
                    break;
                case 15:
                    line = 8;
                    break;
                case 16:
                    line = 9;
                    break;
            }
        }

        for (i in games) {
            if (games.hasOwnProperty(i)) {
                currentClass = '';
                game = games[i];
                if (game.id === currentGameId) {
                    currentClass = ' current-game';
                }
                winnerA = winnerB = '';
                teamA = game.teamA;
                teamB = game.teamB;
                teamAname = teamA ? teamA.name : '?';
                teamBname = teamB ? teamB.name : '?';
                teamAcompName = teamA ? teamA.computerName : 'noteam';
                teamBcompName = teamB ? teamB.computerName : 'noteam';

                ref = game.ref ? game.ref.number : '?';
                sref = game.sref ? game.sref.number : '?';
                jref = '?';

                if (game.jref || game.srefb) {
                    if (game.jref && game.srefb) {
                        jref = game.jref.number + ', ' + game.srefb.number;
                    }
                    else if (game.jref && !game.srefb) {
                        jref = game.jref.number;
                    }
                    else if (!game.jref && game.srefb) {
                        jref = game.srefb.number;
                    }
                }

                pointA = (game.pointA && game.pointA % 1 === 0) ? game.pointA : nopoint;
                pointB = (game.pointB && game.pointB % 1 === 0) ? game.pointB : nopoint;
                classA = classB = 'point-yellow';
                if (pointA > pointB || (pointA !== nopoint && pointB === nopoint)) {
                    classA = 'point-green';
                    classB = 'point-red';
                    winnerA = ' winner';
                }
                else if (pointA < pointB || (pointB !== nopoint && pointA === nopoint)) {
                    classA = 'point-red';
                    classB = 'point-green';
                    winnerB = ' winner';
                }

                if (line && j % line === 0) {
                    currentClass += ' nba'
                }

                if (type === 'S.Nm') {
                    currentClass += ' td-' + logosize;
                }
                else if (currentType === 'S' || currentType === 'RAJ') {
                    currentClass += ' small'
                }

                if (game.gameLength && game.gameLength.computerName) {
                    vs = ' ' + game.gameLength.computerName + ' ';
                }
                else {
                    vs = '-';
                }

                j++;

                content += '<div class="game' + currentClass + '"' + height + '>';

                if (iscurrent) {
                    content += '<div class="game-scores">';
                }
                content += '<div class="teamA border-left-tablee-' + teamAcompName + winnerA + '">';
                content += '<span class="team-size-' + logosize + ' team-logo-left team-logo-left-' + logosize + '-' + teamAcompName + winnerA + '">';
                content += teamAname;
                content += '</span>';
                content += '</div>';

                content += '<div class="vs">';
                content += '<span class="vs-cell point point-left ' + classA + '">' + pointA + '</span>';
                content += '<span class="vs-cell vs-str">' + vs + '</span>';
                content += '<span class="vs-cell point point-right ' + classB + '">' + pointB + '</span>';
                content += '</div>';

                content += '<div class="teamB border-right-tablee-' + teamBcompName + winnerB + '">';
                content += '<span class="team-size-' + logosize + ' team-logo-right team-logo-right-' + logosize + '-' + teamBcompName + winnerB + '">';
                content += teamBname;
                content += '</span>';
                content += '</div>';

                if (iscurrent) {
                    content += '</div>';

                    content += '<div class="game-meta">';
                    content += '<span class="game-meta-data">FB: ' + ref + '</span>';
                    content += '<span class="game-meta-data">SB: ' + sref + '</span>';
                    content += '<span class="game-meta-data">JK: ' + jref + '</span>';
                    content += '</div>';
                }

                content += '</div>';
            }
        }

        content += '</div></div>';

        $('.content', el).html(content);
        $('.widget-title', el).html(title + titleStatus);
    }
};