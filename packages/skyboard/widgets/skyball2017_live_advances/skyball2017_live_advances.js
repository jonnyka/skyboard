widget = {
    onData: function(el, d) {
		var content = '<table id="teams" class="lined-table"><tbody>',
			team,
            tid,
            data = d.data,
            title = d.title,
            type = d.type,
            size = '50',
            settings = d.settings,
            ended = settings.ended,
            currentRound = settings.currentRoundId,
            titleStatus = ended ? ' (végleges)' : ' (nem végleges)';

        if (d.notit) {
            titleStatus = '';
        }

        if (type === 'S.ESs' ) {
            size = '26';
        }


        if (type === 'N.KDs') {
            size = '38';
        }

        currentRound++;
        if (title === 'Továbbjutók') {
            title += ' -> ' + currentRound + '. forduló S';
        }
        if (title === 'Kiesettek') {
            currentRound++;
            title += ' -> ' + currentRound + '. forduló ES';
        }

        for (tid in data) {
            if (data.hasOwnProperty(tid)) {
            	team = data[tid];

                content += '<tr><td class="team-logo-left td-' + size + ' team-logo-left-' + size + '-' + team.computerName + '">' + team.name + '</td></tr>';
            }
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
        $('.widget-title', el).html(title + titleStatus);
    }
};