widget = {
    onData: function(el, d) {
        var content = '<table id="selejtezo" class="lined-table"><thead><tr><th class="centered">"A" csoport</th><th class="centered">"B" csoport</th><th class="centered">"C" csoport</th><th class="centered">"D" csoport</th></tr></thead><tbody><tr>',
            team,
            tid,
            data = d.data,
            title = d.title,
            settings = d.settings,
            size = '50',
            type = d.type,
            ended = settings.ended,
            titleStatus = ended ? ' (végleges)' : ' (nem végleges)',
            i = 1;


        if (type === 'NUL.St') {
            size = '70';
        }

        if (d.notit) {
            titleStatus = '';
        }

        for (tid in data) {
            if (data.hasOwnProperty(tid)) {
                team = data[tid];

                content += '<td class="team-logo-left team-logo-left-' + size + '-' + team.computerName + '">' + team.name + '</td>';
                i++;

                if (i % 4 === 1) {
                    content += '</tr><tr>';
                }
            }
        }

        content += '</tr></tbody></table>';

        $('.content', el).html(content);
        $('.widget-title', el).html(title + titleStatus);
    }
};