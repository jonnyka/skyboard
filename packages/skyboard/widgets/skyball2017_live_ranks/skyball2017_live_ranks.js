widget = {
    onData: function(el, d) {
		var team,
            tid,
            g,
            q,
            ga,
            kq,
            grank,
            name,
            cname,
            line = d.line,
            cl = '',
            data = d.data,
            title = d.title,
            type = d.type,
            settings = d.settings,
            currentRound = settings.currentRoundId,
            gamesNum = parseInt(Object.keys(data).length),
            size = '50',
            ended = settings.ended,
            titleStatus = ended ? ' (végleges)' : ' (nem végleges)',
            i = 1,
            content;

        if (gamesNum >= 12) {
            size = '32';
        }

        if (type === 'N.t' || type === 'KD.t' || type === 'N.KDs' || type === 'RAJ.RAJs') {
            size = '38';
        }

        if (type === 'RAJ.RAJs') {
            switch (currentRound) {
                case 13:
                    line = 8;
                    break;
                case 14:
                    line = 6;
                    break;
                case 15:
                    line = 4;
                    break;
                case 16:
                    line = 2;
                    break;
            }
        }

		content = '<table id="teams" class="lined-table td-' + size + '"><thead><tr><th class="centered">Hely</th>';
		if (type === 'S.Ss') {
            content += '<th class="centered">Cs. hely</th>';
        }
        content += '<th>Csapat</th><th class="centered">Pontszám</th><th class="centered">Gól</th><th class="centered">QS</th><th class="centered">Gólarány</th></tr></thead><tbody>';

        if (d.notit) {
            titleStatus = '';
        }

        for (tid in data) {
            if (data.hasOwnProperty(tid)) {
            	team = data[tid];
                g = team.g ? team.g : 0;
                q = team.q ? team.q : 0;
                ga = team.ga ? team.ga : 0;
                kq = team.kq ? team.kq : 0;
                grank = team.groupRank ? team.groupRank : '?';
                cname = team.computerName ? team.computerName : 'questionmark';
                name = team.name ? team.name : '?';

                cl = '';
                if (line && i === line) {
                    cl = ' class="nba"'
                }

                content += '<tr' + cl +'>';
                content += '<td class="centered">' + i + '</td>';

                if (type === 'S.Ss') {
                    content += '<td class="centered">' + grank + '</td>';
                }

                content += '<td class="team-logo-left team-logo-left-' + size + '-' + cname + '">' + name + '</td><td class="centered"><b>' + team.point + '</b></td>';
                content += '<td class="centered">' + g + '</td><td class="centered">' + q + '</td><td class="centered">' + ga + '</td>';
                content += '</tr>';

                i++;
            }
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
        $('.widget-title', el).html(title + titleStatus);
    }
};