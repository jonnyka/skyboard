widget = {
    onData: function(el, data) {
		var content = '<div id="games"><div class="pull-left">',
			round = data.data.round,
            i,
            j = 0,
            game,
            pointA,
            pointB,
            classA,
            classB,
            teamA,
            teamB,
            teamAname,
            teamBname,
            winnerA,
            winnerB,
            teamAcompName,
            teamBcompName,
            currentClass,
            currentGameId = data.data.currentGame,
            gamesNum = parseInt(data.data.games),
            groupTitlesNum = parseInt(data.data.groupTitles),
            perTwo = parseInt(Math.floor((gamesNum + groupTitlesNum) / 2)),
            height = ' style="height: calc(100% / ' + perTwo + ')"';

		if (round && round.games) {
            $('.widget-title').text('SkyBall 2016 élő (' + round.name + ')');
            
            for (i in round.games) {
                if (round.games.hasOwnProperty(i)) {
                    currentClass = '';
                    game = round.games[i];
                    if (game.id == currentGameId) {
                        currentClass = ' current-game';
                    }
                    winnerA = winnerB = '';
                    teamA = game.teamA;
                    teamB = game.teamB;
                    teamAname = teamA ? teamA.name : '?';
                    teamBname = teamB ? teamB.name : '?';
                    teamAcompName = teamA ? teamA.computerName : 'noteam';
                    teamBcompName = teamB ? teamB.computerName : 'noteam';
                    pointA = game.pointA ? game.pointA : '0';
                    pointB = game.pointB ? game.pointB : '0';
                    classA = classB = 'point-yellow';
                    if (pointA > pointB) {
                        classA = 'point-green';
                        classB = 'point-red';
                        winnerA = ' winner';
                    }
                    else if (pointA < pointB) {
                        classA = 'point-red';
                        classB = 'point-green';
                        winnerB = ' winner';
                    }

                    if (j == perTwo) {
                        content += '</div><div class="pull-right">'
                    }

                    if (game.gameType.groupTitleTrans) {
                        content += '<div class="group-title"' + height + '><span class="title">' + game.gameType.groupTitleTrans + '</span></div>';
                        j++;
                    }

                    content += '<div class="game' + currentClass + '"' + height + '>';
                        content += '<div class="teamA border-left-table-' + teamAcompName + winnerA + '">';
                            content += '<span class="team-logo-left team-logo-left-table-' + teamAcompName + winnerA + '">';
                                content += teamAname;
                            content += '</span>';
                        content += '</div>';

                        content += '<div class="vs">';
                            content += '<span class="vs-cell point ' + classA + '">' + pointA + '</span>';
                            content += '<span class="vs-cell vs-str">-</span>';
                            content += '<span class="vs-cell point ' + classB + '">' + pointB + '</span>';
                        content += '</div>';

                        content += '<div class="teamB border-right-table-' + teamBcompName + winnerB + '">';
                            content += '<span class="team-logo-right team-logo-right-table-' + teamBcompName + winnerB + '">';
                                content += teamBname;
                            content += '</span>';
                        content += '</div>';
                    content += '</div>';

                    j++;
                }
            }
        }

        content += '</div></div>';

        $('.content', el).html(content);
    }
};