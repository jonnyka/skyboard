widget = {
    onData: function(element, data) {
        var mainTitle = $('.title'),
            time = data.data;

        if (!mainTitle.find('.time').length) {
            mainTitle.html(mainTitle.html() + '<span class="time">' + time + '</span>');
        }
        mainTitle.find('.time').text(time);
    }
};