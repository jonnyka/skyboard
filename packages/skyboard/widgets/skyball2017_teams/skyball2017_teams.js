widget = {
    onData: function(el, data) {
		var content = '<table id="teams" class="lined-table"><thead><tr><th>Csapat</th><th>Hely</th><th>Pont</th></tr></thead><tbody>',
			team,
            tid,
            rank,
            i = 1;

        for (tid in data.data) {
            if (data.data.hasOwnProperty(tid)) {
            	team = data.data[tid];
            	rank = team.rank;

            	if (!rank) {
                    rank = i;
                }
                i++;

                content += '<tr>';
                content += '<td class="team-logo-left team-logo-left-38-' + team.computerName + '">' + team.name + '</td><td>' + rank + '</td><td>' + team.point + '</td>';
                content += '</tr>';
            }
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
    }
};