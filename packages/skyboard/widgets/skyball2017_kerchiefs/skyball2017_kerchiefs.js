widget = {
    onData: function(el, data) {
        var kcTypes = data.data.kcTypes,
            kerchiefs = data.data.kerchiefs,
            players = data.data.players,
            kct,
            kcType,
            kc,
            kerchief,
            pid,
            rpid,
            value,
            teamname,
            number,
            name,
            nul = '-',
            i,
            j = 0,
            content = '<table id="kerchiefs" class="lined-table"><thead><tr><th></th>';

        for (kct in kcTypes) {
            if (kcTypes.hasOwnProperty(kct)) {
                kcType = kcTypes[kct];

                content += '<th class="centered kcth kerchief-' + kcType.kerchiefCategory.computerName + '">' + kcType.computerName + '<span class="kc-small">' + kcType.name + '</span></th>';
            }
        }

        content += '</tr></thead><tbody>';

        for (i = 0; i < 10; i++) {
            content += '<tr' + ((i === 2) ? ' class="nba"' : '') + '><td class="centered"><b>' + (i + 1) + '</b></td>';

            for (kc in kerchiefs) {
                if (kerchiefs.hasOwnProperty(kc)) {
                    kerchief = kerchiefs[kc];
                    j = 0;

                    for (pid in kerchief) {
                        if (kerchief.hasOwnProperty(pid)) {
                            value = (kerchief[pid] === 0) ? nul : kerchief[pid];
                            rpid = pid.replace('p', '');
                            number = (value !== nul) ? players[rpid].number : '';
                            name = (value !== nul) ? players[rpid].name : '';
                            teamname = players[rpid].team.computerName;

                            if (j === i) {
                                content += '<td class="centered round-player-td">' +
                                    '<span class="player-number team-color-' + teamname + '">' + number + '</span>' +
                                    '<span class="player-name team-color-' + teamname + '">' + name + '</span>' +
                                    '<span class="player-score">' + value + '</span></td>';
                            }
                        }

                        j++;
                    }
                }

            }

            content += '</tr>';
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
    }
};