widget = {
    onData: function(el, data) {
		var content = '<table id="teams" class="lined-table"><thead><tr><th>Csapat</th><th>Hely</th><th>Pont</th></tr></thead><tbody>',
			team,
            tid;

        for (tid in data.data) {
            if (data.data.hasOwnProperty(tid)) {
            	team = data.data[tid];

                content += '<tr>';
                content += '<td class="team-logo-left team-logo-left-' + team.computerName + '">' + team.name + '</td><td>' + team.rank + '</td><td>' + team.point + '</td>';
                content += '</tr>';
            }
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
    }
};