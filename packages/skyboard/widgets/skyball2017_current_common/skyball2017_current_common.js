widget = {
    onData: function (el, data) {
        var serverDate = moment(data.date),
            date = serverDate.format(data.dateFormat);

        $('.date', el).html(date);
    }
};