widget = {
    onData: function(el, data) {
		var content = '<div id="games"><div class="pull-left">',
			round = data.data,
            i,
            j = 0,
            k = 0,
            game,
            pointA,
            pointB,
            classA,
            classB,
            teamA,
            teamB,
            teamAname,
            teamBname,
            winnerA,
            winnerB,
            teamAcompName,
            teamBcompName,
            currentClass,
            size = 32,
            vs = '-',
            ccc = '',
            settings = data.settings,
            currentGameId = settings.currentGameId,
            gamesNum = round.games.length,
            groupTitlesNum = round.groupTitles,
            perTwo = parseInt(Math.ceil((gamesNum + groupTitlesNum) / 2)),
            height = ' style="height: calc(100% / ' + perTwo + ')"';
            //height = '';

		if (round && round.games) {
            $('.widget-title').text('Water SkyBall 2017 - ' + round.name);

            for (i in round.games) {
                if (round.games.hasOwnProperty(i)) {
                    currentClass = '';
                    game = round.games[i];
                    if (game.id == currentGameId) {
                        currentClass = ' current-game';
                    }
                    winnerA = winnerB = '';
                    teamA = game.teamA;
                    teamB = game.teamB;
                    teamAname = teamA ? teamA.name : '?';
                    teamBname = teamB ? teamB.name : '?';
                    teamAcompName = teamA ? teamA.computerName : 'noteam';
                    teamBcompName = teamB ? teamB.computerName : 'noteam';
                    pointA = game.pointA ? game.pointA : '?';
                    pointB = game.pointB ? game.pointB : '?';
                    classA = classB = 'point-yellow';
                    if (pointA > pointB) {
                        classA = 'point-green';
                        classB = 'point-red';
                        winnerA = ' winner';
                    }
                    else if (pointA < pointB) {
                        classA = 'point-red';
                        classB = 'point-green';
                        winnerB = ' winner';
                    }

                    if (game.gameLength && game.gameLength.computerName) {
                        vs = ' ' + game.gameLength.computerName + ' ';
                    }
                    else {
                        vs = '-';
                    }

                    if (j == perTwo) {
                        content += '</div><div class="pull-right">'
                    }

                    if (game.gameType.groupTitle && game.gameType.groupTitle === 'gameType.selejtezok') {
                        k++;
                    }
                    if (game.gameType.groupTitle && game.gameType.groupTitle === 'gameType.normal') {
                        k = 0;
                    }

                    if (k) {
                        k++;

                        if (k % 3 === 1) {
                            currentClass += ' nba';
                        }
                    }

                    if (game.gameType.groupTitleTrans) {
                        ccc = '';
                        if (game.gameType.groupTitle === 'gameType.selejtezok') {
                            ccc = ' nba';
                        }
                        content += '<div class="group-title' + ccc + '"' + height + '><span class="stitle">' + game.gameType.groupTitleTrans + '</span></div>';
                        j++;
                    }

                    content += '<div class="game' + currentClass + '"' + height + '>';
                        content += '<div class="teamA border-left-tablee-' + teamAcompName + winnerA + '">';
                            content += '<span class="team-logo-left team-logo-left-' + size + '-' + teamAcompName + winnerA + '">';
                                content += teamAname;
                            content += '</span>';
                        content += '</div>';

                        content += '<div class="vs">';
                            content += '<span class="vs-cell point ' + classA + '">' + pointA + '</span>';
                            content += '<span class="vs-cell vs-str">' + vs + '</span>';
                            content += '<span class="vs-cell point ' + classB + '">' + pointB + '</span>';
                        content += '</div>';

                        content += '<div class="teamB border-right-tablee-' + teamBcompName + winnerB + '">';
                            content += '<span class="team-logo-right team-logo-right-' + size + '-' + teamBcompName + winnerB + '">';
                                content += teamBname;
                            content += '</span>';
                        content += '</div>';
                    content += '</div>';

                    j++;
                }
            }
        }

        content += '</div></div>';

        $('.content', el).html(content);
    }
};