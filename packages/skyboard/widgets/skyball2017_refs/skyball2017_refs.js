widget = {
    onData: function(el, data) {
		var content = '<table id="teams" class="lined-table"><thead><tr><th class="centered">#</th><th>Játékos</th><th class="centered">Összpontszám</th><th class="centered">Mérkőzések száma</th><th class="centered">Átlag pontszám</th></tr></thead><tbody>',
			player,
            pid,
            title = data.title,
            className = '',
            i = 1,
            limit = 8;

        for (pid in data.data) {
            if (data.data.hasOwnProperty(pid)) {
                if (i <= limit) {
                    player = data.data[pid];
                    className = player.sex === 'm' ? 'sex-male' : 'sex-female';

                    content += '<tr>';
                    content += '<td class="centered">' + i + '</td>';

                    content += '<td class="round-player-td ' + className + '">' +
                        '<span class="player-number">' + player.number + '</span>' +
                        '<span class="player-name">' + player.name + '</span>' +
                        '</td>';

                    content += '<td class="centered">' + player.point + '</td>';
                    content += '<td class="centered">' + player.rnumber + '</td>';
                    content += '<td class="centered">' + player.avg + '</td>';
                    content += '</tr>';

                }

                i++;
            }
        }

        content += '</tbody></table>';

        $('.content', el).html(content);
        $('.widget-title', el).html(title);
    }
};