widget = {
    onData: function (el, data) {
        var serverDate = moment(data.date),
            date = serverDate.format(data.dateFormat),
            time = serverDate.format(data.timeFormat);

        $('.date', el).html(date);
        $('.time', el).html(time);
    }
};