var ws = require("nodejs-websocket");

var server = ws.createServer(function (conn) {
    conn.on('text', function (str) {
        broadcast(str);
    });

    conn.on('close', function (code, reason) {});
}).listen(8080);

function broadcast(str) {
    server.connections.forEach(function (connection) {
        connection.sendText(str);
    });
}
